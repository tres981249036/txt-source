出了家門後，我背著自稱神劍的直劍，前往集合的目的地。

幸好外表看上去就是隨處可見、極奇普通的直劍，所以誰都沒有在意。
因為廣場上的神劍消失了，好像引起了小騷動的樣子。

「啊，終於來了。太晚了──誒？⋯⋯你是誰？」

到約好碰面的地點，隊伍裡的其他三個人已經在那裡等候多時了。
因為從早上開始就發生各種讓我驚嘆的怪事，所以比平常出門稍微遲了些。

但我究竟應該說些什麼，見到我的三個人紛紛露出了一副

「這是誰？」、「啊？不是大叔啊？」的表情。

看來我的外表看起來太年輕了，他們似乎沒有意識到其實是我本人。

我突然想到了一個好主意。

「不好意思、我家哥哥一直受到你們照顧了。」
「那傢伙，原來有弟弟的嗎？」

我決定是弟弟這樣的設定。

這樣即使長相相似似乎也說得過去。

然後順便告訴他們，今天我（老盧卡斯）不會過來參加組隊冒険。
比起和這些傢伙一起冒険，我更想儘快確認這把劍的性能。

「開什麼玩笑！⋯⋯區區提行李的還敢放鴿子」
「啊，雷克。要不乾脆把那傢伙給開除吧？」

他們不知道本人在眼前的話，就會說出這樣性質惡劣的壊話。
不，就算我真的在他們眼前也會照樣罵吧⋯⋯

『汝，還真是被這群過份的年輕人當成笨蛋了吶』

⋯⋯很遺憾，就是這樣的。

「嘛，沒辦法了。今天就我們幾個去吧。」

對此只有瑪麗有所理解的樣子，並安慰著雷克他們。

這傢伙是這麼通情達理的性格來著？
平時的話大概是「哈，大叔真沒用，去死吧。」這樣毫不留情地說我。

「謝謝你將這事告訴我們呢。我的名字是瑪麗。誒多，你的名字是⋯⋯？」

不知道是怎麼回事，在問我的名字嗎？

「我叫⋯⋯魯卡。」

我使用了假名。

「帶著武器啊，難道魯卡桑也是冒険者嗎？」
「啊，姑且算是。」
「是這樣啊，那如果下次有機會的話，一定要一起冒険一下呢。呵呵。」

（插圖００２－０１１）

⋯⋯不不不，你是誰啊？

『這不是個很可愛的女孩子嗎？』

嗯，只是表面上看起來呢。

對於與往常完全不同的態度的瑪麗，我抱著困惑的心情，離開了那裡。

───

在加入雷克那群小鬼們的隊伍之前，我也有著哥布林殺手這項別名。
當然，這不是讚美的評價。

只會狩獵目前所知最弱的魔物哥布林，而一直被嘲笑著。

年輕的時候，即使在一對一的情況下也能夠打倒豬頭人。
根據冒険者公會制定的危險級別來看，豬頭人危險度是Ｄ級。
意思就是，如果是身為Ｄ級的冒険者，也可以單獨對抗的程度。

但是隨著年齡的增長，我的主要狩獵對象從危險程度為Ｅ等上級的狗頭人

變成了Ｅ等下級的哥布林，之後逐漸成為了低水平的冒険者。

說實話，大概是連Ｄ級的實力都不到吧⋯⋯⋯

但是如果是現在，我的傷都已經痊癒了，也許還可以試試討伐豬頭人。
為此我來到了豬頭人經常出沒的森林地帶。

「⋯⋯有了！」

Lucky！馬上就發現了豬頭人。

更值得慶幸的是它只有一隻。
豬頭人和天性群居的哥布林、狗頭人不同，
但是偶爾也會有幾隻喜歡複數行動的個體出現。

「噗噗？」

不知道是否是因為嗅覺察覺到了我的氣息，豬頭人的鼻子哼哼作響地開始環視四周。

趁著沒有被發現之前，我從樹的陰影裡沖了出來。
聽到了來自背後的腳步聲，豬頭人轉了過來。

但是，太慢了！

我用力將劍砍向了豬頭人的右肩。

「誒？」

從劍的那端傳來了像是切黃油般的手感。

豬頭人比起狗頭人和哥布林，皮應該是是比較厚的才對。

但令人吃驚的是，刀刃卻輕易的刺了進去。
所以劍尖就這樣一路到了豬頭人的左側切了開來。

身體被斜著切斷的豬頭人，瞪大了眼睛後，便斃命了。

「⋯⋯這麼輕易的就戰勝了嗎？」

豬頭人的身體逐漸化為灰燼崩潰了。

與人類和動物不同，魔物死掉後，遺體不會保持原來的形狀而化為灰燼。

『倒不如說，如果只是豬頭人這種程度都不能輕鬆討伐的話才奇怪吶？』
「不，這對我來說算是很不得了的事啊⋯⋯」

畢竟，在遇到神劍之前，連狗頭人作為對手都會十分地費勁呢。

『那就稍微來解釋下我的性能吧。』
「性能？」
『唔姆，首先單指攻擊力的話，我是遠遠高於那些另類的名劍的。』

豬頭人意外的肌肉很多，因此肉比較硬，相對來說比較難以造成傷害。

連肌肉也能徹底地斬斷，那麼是不可能有什麼低等級的攻擊力的吧。

『再加上，身體能力、自然治癒能力的提升、中毒和麻痺等異常狀態耐性的上升
武運上升、還有幸運色狼⋯⋯之類的特殊效果也跟著增幅到了你身上。』

雖然很在意說的最後一句話，但是其他的不管哪一項都有驚人的性能。

而且原本這些特殊效果，只有部分高級武器和魔導具才會持有
並不是像我這種貧窮的冒険者所能得到的。

而且一個武具最多也就只能持有一個特殊效果。

『但是，最值得一提的是【固有能力】的〈眷姫後宮〉。』
「〈眷姫後宮〉⋯⋯？」
『沒錯，這才是愛與勝利的女神維妮婭所產生出的，被稱作是吾真正精髓所在的能力！』
「具體地來說是？」
『簡單來說，汝娶的妻子越多，我的性能就越強！』

⋯⋯⋯⋯

這傢伙，到底在胡些說什麼啊？