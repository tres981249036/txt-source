１０

「師阿。師阿。昨天，真的聽了很讓人感興趣的話。但是愚昧的我，還不清處何謂藥草。請再更深一步教導我吧」
「也是呢。昨天，從土、草、人的不同說到了生命，今天就從突破那的境界開始，來想想何謂生命吧。你知道鹽吧」
「冒昧的是，說不上理有解那本質」
「迴避的真漂亮呢。鹽以昨天的區分來看就是土。還行嗎」
「理解了」
「昨天說了，草會吃土，而人會吃草。這沒有錯，但人之生命欠缺鹽。這是怎麼一回事呢」
「師阿。鹽支撐著固定人肉，讓人動作的力量之源。過去，師是這麼教我的」
「那是正確的知識。同時也是表面的知識。先暫時忘了吧。然後思考看看。如果人成了完全的存在，就不需要攝入什麼，也不需要捨棄什麼。金錢正好就像這樣呢」
「但是人會進食，排泄。這是人並不完全的證明嗎」
「是如此，也不是如此。仔細想想吧。金錢不會變。沒有得失的話，就不會衰退也不會成長。雖然這說法不定是完美的，但其中有真正的美麗和真正的機能嗎。你覺得，雖然美麗又最有機能，但不能變得更美更有機能的東西，跟為了變得更美更有機能而捨去現在的自己的東西，哪邊才是真正美麗有機能的呢」
「我覺得，往上追求的才美麗。追求更好的，我覺得更有機能」
「人就是如此。生命就是如此阿。人是會成長的存在。那是凌駕了完全的完全。那麼生命的特徵是什麼呢。種植了從同一株草採下的種子，會在不同的位置生出樹枝。也就是說，生命是變動的。那變動，才是活物最明確的證明喔」
「變動是什麼呢。生命又是什麼呢」
「變動，是事物不去選擇就會被選擇，但在這場合有另一個意思。人雖然持續活著，但各個部分又會持續死去。然後會持續誕生。人體並非固定的，會隨時被新物取代，把自己改寫喔」
「這是什麼意思呢」

記錄員不斷沙沙沙地刻著文字。


１１

「失禮了」

領主家的執事進來了。

「午餐準備好了。可以的話，能請各位移動嗎」

一行人移動到了食堂。不過，也只是近在旁邊的房間。

椅子只留了兩張，其他都被移走了。斯卡拉貝爾和希拉以外的人要站著吃。
只有在這午餐時間，隨行的神官和藥師也能進入迎賓館。之所以如此，是顧慮到不能進入談話室的人們，給予參與兩人的對話的機會。

雷肯等人進入食堂時，神官和藥師全員都到齊了在等待。
然後，他們都震驚的失語了。
年老得步伐蹣跚的斯卡拉貝爾，正快步走著。
不只如此。蒼白的臉色，還取回了人的健康肉色。
究竟發生了什麼。
集中了太多注目，得要有誰說些什麼。

在寂靜的食堂裡開口的是，亞馬密爾。

「在與希拉大人的對談中，開啟了至今沒有人注意到的〈淨化〉的嶄新可能性。拜此所賜，斯卡拉貝爾導師大大取回了健康。向大神獻上感謝吧」

喧鬧四起。

有人劃了聖印，有人流下了淚，有人靠近斯卡拉貝爾跪了下來，在近距離看著和昨天明顯不同的斯卡拉貝爾。
也有人纏上亞馬密爾，要求說明發生了什麼。

大家都被驚訝、喜悅與感動籠罩。那也顯示了，斯卡拉貝爾有多受仰慕。
斯卡拉貝爾問了希拉要吃什麼，希拉回答要沒有配料的湯和切過的三明治。斯卡拉貝爾便自己走向料理桌，把三明治放上盤子，跟女僕拿了湯，放到希拉麵前。然後也為自己準備了同樣的。
亞馬密爾神官向看著安靜地吃飯的兩人的雷肯搭了話。

「師叔」
「別用那個稱呼了」
「哈哈。也是呢，雷肯殿」
「怎麼了」
「請問，體力回復藥的材料，還有嗎？」
「嗯？還有做幾個的份量吧」
「雖然非常失禮，但能麻煩在大家面前，展現調藥的技術嗎」
「亞馬密爾」

斯卡拉貝爾發出了像是要規勸的聲音。

「沒關係喔」
「師阿」
「有這麼多的藥師，大老遠從王都過來。也不能不給任何招待阿。雷肯」
「啊啊」
「魔力回復藥的素材也還有吧？」
「啊啊」
「好。那麼，各位。趕快填飽肚子吧。等桌上的料理整理完後，雷肯就會做體力回復藥和魔力回復藥給各位看。我最厲害的技術有傳授給雷肯。雷肯做的藥，能看作跟我做的藥相同喔」

（不可能作出跟你的相同的藥吧）

雖然不希望她說這種挑起周圍的期待的話，但這下也不能就此退出了。

雷肯猛烈地走向料理。
把所有煙燻肉移到盤子裡，大口咀嚼。
把整塊三明治移到盤子裡，一口吞下。
咬碎生菜，把湯喝光，做好了戰鬥準備。
料理被收拾乾淨，多餘的桌子也被搬走了。

雷肯站到牆壁旁，前面放了張小桌子。
斯卡拉貝爾和希拉坐在放在正前方的兩張椅子上。
其他人則站著，把桌子圍繞得緊緊的。

在最前列的叫瑪露莉亞的女神官的眼神很可怕。
是聽到傳聞了嗎，騎士團的副團長和兩位神殿騎士也進來了。

三位魔法使遲了點，也進來了。

（嘖）
（來了多餘的人了）

雷肯接下來要灌注全心全力來披露魔力控制。讓藥師看是無所謂。但是，被魔法使看到的話，可能會暴露本領。雖然如此，也已經沒後路了。

（算了）
（要看就看吧）

取出小碟子，取出深皿，取出素材，雷肯首先做了體力回復藥。
接著，做了魔力回復藥。
完成的魔力回復藥，掉入深皿發出輕聲時，食堂一片寧靜，沒有人發出一絲聲響。宛如大家連呼吸都忘了。

不久後，有人深深吐了氣，有人拍了手。
然後，房間裡的人們，贈與雷肯毫不吝嗇的稱讚之拍手。

藥師卡溫協助了斯卡拉貝爾。斯卡拉貝爾從椅子上站起，向雷肯深深低下了頭。

「之前覺得你是師弟，但並非如此。你原來是我的師兄阿」
「斯卡拉貝爾導師。沒那種事吧。你能做出比我做的還好上許多的藥才對」
「其實，聽到你修得了調藥和魔法兩方時，感覺有些奇異。因為那兩個是沒辦法並存的。但是，看了剛剛的調藥我才知道。最高峰的調藥，是需要最高峰的魔力控制的阿」
「把頭抬起來吧，導師。我是才剛學了藥的作法的雛鳥。是對治癒人的傷痛疾病一無所知的新手」
「但是，如果沒有你在，師的最高技術就會失傳。雖然對為什麼能在那年紀做的那麼精密的控制感到非常不可思議，但如果不是你，就繼承不了這技術吧。我是在為這件事低頭」
「可以了。雷肯很困擾喔。那麼，回剛才的房間去吧。平時都一個人，跟一堆人在一起可累了阿」
「雷肯殿。能收下這兩個藥嗎」
「啊啊」
「各位，我們要回談話室了。這藥會放在這裡，好好觀察吧」

瑪露莉亞和另一位神官，恭敬地抬起放了體力回復藥和魔力回復藥的深皿。
雷肯雖然想收回深皿，但在這氣氛又不太好說出來，便保持沉默了。

他們只能在午餐時間待在這裡，之後會回領主館，必須在傍晚回作為宿舍的貴族家。所以，接下來會在領主館仔細地觀察體力回復藥和魔力回復藥吧。